CFLAGS += -Wall -Wextra -Ofast -pthread
LIBS = `pkg-config --cflags --libs alsa ncurses`

all:
	gcc $(CFLAGS) -o qlab-alsaloop src/qlab-alsaloop.c $(LIBS)
	gcc $(CFLAGS) -o info src/info.c $(LIBS)
	gcc $(CFLAGS) -o qmixer src/qmixer.c $(LIBS)

