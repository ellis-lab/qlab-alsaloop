#!/usr/bin/env python3
# this is another testing script for pillar work
import matplotlib.pyplot as plt
import numpy as np
from scipy.signal import find_peaks
import time
import qlab_alsaloop
import sys

feedback = qlab_alsaloop.Feedback()

# remember: want this to be more than 192 (i.e. 2pi)
psize = 256

fs = 192000

feedback.set_psize(psize)
feedback.set_gain(0, 0, 0, 0)
feedback.set_filter(0.15, 0, 0.85, 0.15, 0, 0.85)
feedback.send_parameters()

# scheduler :3
time.sleep(1)
print(np.sum(np.abs(np.fft.rfft(np.random.random(2**18)))))
time.sleep(1)
print("done fucking with scheduling")

xi,xo = feedback.tune_coarse_phase(feedback.AXIS_XX, 965, periods=32)
yi,yo = feedback.tune_coarse_phase(feedback.AXIS_YY, 965, periods=32)

feedback.set_delay(xi, 0, 0, yi-15)
feedback.set_gain(-0.6, 0, 0, 1)
feedback.send_parameters()
time.sleep(5)

ding = np.zeros(psize).astype(np.int32)
nulls = np.zeros(psize).astype(np.int32)
for i in range(100):
    ding[i] = 2**31-1

for i in range(1):
    time.sleep(2)
    rec = feedback.playrec_frames((ding, nulls), nperiods=2048)
    fftx = np.fft.rfft(rec[0][600:])
    ffty = np.fft.rfft(rec[1][600:])
#   fftx = np.fft.rfft(rec[0][300000:])
#   ffty = np.fft.rfft(rec[1][300000:])
    if i == 0:
        sum_fftx = fftx[0:len(fftx)//32]
        sum_ffty = ffty[0:len(ffty)//32]
    else:
        sum_fftx += fftx[0:len(fftx)//32]
        sum_ffty += ffty[0:len(ffty)//32]

sum_fftx = np.abs(sum_fftx)
sum_ffty = np.abs(sum_ffty)

plt.plot(rec[0])
plt.plot(rec[1])
plt.show()

lb = 950*2*32*len(sum_fftx)//fs
ub = 990*2*32*len(sum_fftx)//fs
print(lb)
peaksx = find_peaks(sum_fftx, height=1)
peaksy = find_peaks(sum_ffty, height=1)
main_peaksx = peaksx[0][peaksx[1]["peak_heights"].argsort()]
main_peaksx = main_peaksx[(lb < main_peaksx) & (main_peaksx < ub)][-2:]
print(main_peaksx)
main_peaksy = peaksy[0][peaksy[1]["peak_heights"].argsort()]
main_peaksy = main_peaksy[(lb < main_peaksy) & (main_peaksy < ub)][-2:]
plt.plot(np.fft.fftfreq(sum_fftx.size, d=64/fs), sum_fftx)
plt.plot(np.fft.fftfreq(sum_ffty.size, d=64/fs), sum_ffty)
plt.scatter(main_peaksx*fs/2/32/len(sum_fftx), sum_fftx[main_peaksx], marker='x')
plt.scatter(main_peaksy*fs/2/32/len(sum_ffty), sum_ffty[main_peaksy], marker='x')
plt.yscale("log")
plt.show()
time.sleep(10000)

