#!/usr/bin/env python3
# this program tests our ability to cancel out imbalances
import math
import matplotlib.pyplot as plt
import numpy as np
import time
import qlab_alsaloop
import sys

feedback = qlab_alsaloop.Feedback()

feedback.set_psize(256)
feedback.set_gain(0, 0, 0, 0)
feedback.set_filter(1, 0, 0, 1, 0, 0)
feedback.send_parameters()
time.sleep(2)
feedback.set_gain(0, 1, -1, 0)
feedback.send_parameters()
time.sleep(5)
feedback.tune_coarse_phase()
time.sleep(5)

# coarse skew tuning
fomdict = {}
for i in range(32):
    feedback.set_delay(0, i)
    feedback.send_parameters()
    feedback.start_hold_amp(8, tolerance=0.05, step_xy=0.05, step_yx=-0.05)
    time.sleep(10)
    feedback.stop_hold_amp()
    # we want xy inner product to be zero
    x, y = map(lambda a: np.array(a,dtype="f8"), feedback.get_frames(40))
    xxip = float(np.dot(x, x))
    yyip = float(np.dot(y, y))
    xyip = float(np.dot(x, y))
    print(xxip)
    print(yyip)
    print(xyip)
    fomdict[i] = abs(xyip) / (xxip*xxip + yyip*yyip)**0.5
    print("dy of {} got fom of {}".format(i, fomdict[i]))
bestdy = min(fomdict, key=fomdict.get)
print("setting dy to " + str(bestdy))
feedback.set_delay(0, bestdy)
feedback.send_parameters()
time.sleep(5)

# amplitude tuning
feedback.start_hold_amp(8, tolerance=0.05, step_xy=0.05, step_yx=-0.05)
time.sleep(20)
feedback.stop_hold_amp()
feedback.start_hold_amp(8, tolerance=0.05, step_xy=0.01, step_yx=-0.01)
time.sleep(40)
feedback.stop_hold_amp()
feedback.start_hold_amp(8, tolerance=0.02, step_xy=0.005, step_yx=-0.005)
time.sleep(20)
feedback.stop_hold_amp()

frames = feedback.get_frames(4096)
plt.plot(*frames)
plt.show()
print("recording done! exit whenever.")

time.sleep(60*5)

