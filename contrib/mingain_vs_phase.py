#!/usr/bin/env python3
# this program finds the minimum gain to maintain an amplitude as a function of
# the phase, then plots a family of curves with different held amplitudes
# (saturated, slightly below saturated, and so on)
import csv
import time
import qlab_alsaloop

feedback = qlab_alsaloop.Feedback()

feedback.set_psize(280)
feedback.set_gain(1, 0, 0, 0)
feedback.set_filter(1, 0, 0, 1, 0, 0)
feedback.send_parameters()
time.sleep(2)

header = ["target_amp", "real_amp", "psize", "gain"]

# TODO: fix python thread bugs

with open("mingain_vs_phase.csv", "w", newline="") as csvfile:
    writer = csv.writer(csvfile, dialect="unix")
    writer.writerow(header)
    # psize is equivalent to delay
    for psize in range(280, 280+35+1):
        print("===== NEW PERIOD =====")
        for amp in range(6, 26+1, 4):
            print("===== NEW AMPLITUDE =====")
            print(psize)
            feedback.set_psize(psize)
            feedback.send_parameters()
            # pass 1
            feedback.start_hold_amp(amp, tolerance=0.05, step_xx=0.1,
                step_xy=0, step_yx=0, step_yy=0, wait_time=0.5
            )
            time.sleep(8)
            # pass 2
            feedback.stop_hold_amp()
            feedback.start_hold_amp(amp, tolerance=0.02, step_xx=0.01,
                step_xy=0, step_yx=0, step_yy=0, wait_time=0.5
            )
            time.sleep(20)
            # pass 3
            feedback.stop_hold_amp()
            feedback.start_hold_amp(amp, tolerance=0.01, step_xx=0.001,
                step_xy=0, step_yx=0, step_yy=0, wait_time=0.5
            )
            time.sleep(40)
            # pass 4
            feedback.stop_hold_amp()
            feedback.start_hold_amp(amp, tolerance=0.005, step_xx=0.0002,
                step_xy=0, step_yx=0, step_yy=0, wait_time=0.5
            )
            time.sleep(30)
            reached_amp = 0
            reached_gain = 0
            for _ in range(10):
                time.sleep(2)
                reached_amp += 1/10 * feedback.get_rms_mv()[0]
                reached_gain += 1/10 * feedback.get_gain()[0]
            feedback.stop_hold_amp()
            writer.writerow([amp, reached_amp, psize, reached_gain])

