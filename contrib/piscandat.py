#!/usr/bin/env python3
import sounddevice as sd
import numpy as np

fs = 192000 # sample rate Hz
dt = 0.1    # time to spend scanning each f (seconds)
A = 0.1     # amplitude of output signal (I think 1 is max?)

# simple frequency sweep returning magnitude of recording

recordings = []

for f in np.arange(7100, 7200):
    signal = A * np.cos(np.linspace(0, 2*np.pi*f*dt, int(dt*fs)))
    rec = sd.playrec(signal, channels=1, dtype="float32", blocking=True)
    rec = rec.flatten()
    recordings.append(rec)
    print("f = {} -> amplitude = {}".format(f, np.sqrt(np.dot(rec, rec))))

