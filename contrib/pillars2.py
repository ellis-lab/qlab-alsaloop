#!/usr/bin/env python3
# this is the testing script for pillar work
import matplotlib.pyplot as plt
import numpy as np
import time
import qlab_alsaloop
import sys

feedback = qlab_alsaloop.Feedback()

psize = 128

feedback.set_psize(psize)
feedback.set_gain(0, 0, 0, 0)
feedback.set_filter(0.15, 0, 0.85, 0.15, 0, 0.85)
feedback.send_parameters()
time.sleep(0.5)
xi,xo = feedback.tune_coarse_phase(feedback.AXIS_XX, 965)
xi,xo = feedback.tune_coarse_phase(feedback.AXIS_XX, 965)
yi,yo = feedback.tune_coarse_phase(feedback.AXIS_YY, 965)

feedback.set_delay(xi, 0, 0, yi)
#feedback.set_gain(-1, 0, 0, 5)
#feedback.set_recording(100)
#feedback.send_parameters()
#time.sleep(10)

fft = np.zeros(psize//2*300+1)
for i in range(10):
    feedback.set_gain(-1, 0, 0, 5)
    fft += np.abs(np.fft.rfft(feedback.get_frames(300)[0]))
    feedback.send_parameters()
    time.sleep(1)
    feedback.set_gain(0, 0, 0, 0)
    feedback.send_parameters()
    time.sleep(1)

plt.plot(fft)
plt.yscale("log")
#plt.plot(recy)
plt.show()
time.sleep(10000)

