#!/usr/bin/env python3
# this program tests the qlab-alsaloop recording feature
import matplotlib.pyplot as plt
import numpy as np
import time
import qlab_alsaloop

psize = 256
feedback = qlab_alsaloop.Feedback()

feedback.set_psize(psize)
feedback.set_gain(0, 0, 0, 0)
feedback.set_filter(1, 0, 0, 1, 0, 0)
feedback.send_parameters()
time.sleep(1)

F0_SAMP = 960.2 / 192000
play_periods=5120
phi = np.arange(play_periods*psize) * 2*np.pi*F0_SAMP
x = (2**30 * np.cos(phi)).astype(np.int32)
y = np.zeros(play_periods*psize).astype(np.int32)

x, y = map(lambda a: np.array(a,dtype="f8"),
    feedback.playrec_frames((x,y), nperiods=play_periods)
)

#x, y = map(lambda a: np.array(a,dtype="f8"), feedback.get_frames(4))

#line = 4E9/192 * np.arange(len(x))
#resid = x - line

del feedback

#plt.plot(resid, "o-")
plt.plot(x, "o-")
plt.plot(y, "o-")
plt.show()

