#!/usr/bin/env python3
# this is another testing script for pillar work
import matplotlib.pyplot as plt
import numpy as np
from scipy.signal import find_peaks
import time
import qlab_alsaloop
import sys

feedback = qlab_alsaloop.Feedback()

# remember: want this to be more than 192 (i.e. 2pi)
psize = 256

fs = 192000

feedback.set_psize(psize)
feedback.set_gain(0, 0, 0, 0)
feedback.set_filter(0.15, 0, 0.85, 0.15, 0, 0.85)
feedback.send_parameters()

# scheduler :3
time.sleep(1)
print(np.sum(np.abs(np.fft.rfft(np.random.random(2**18)))))
time.sleep(1)
print("done fucking with scheduling")

ding = np.zeros(psize).astype(np.int32)
nulls = np.zeros(psize).astype(np.int32)
for i in range(100):
    ding[i] = 2**31-1

xi,xo = feedback.tune_coarse_phase(feedback.AXIS_XX, 965, periods=32)
yi,yo = feedback.tune_coarse_phase(feedback.AXIS_YY, 965, periods=32)
feedback.set_delay(xi, 0, 0, yi)
feedback.send_parameters()

# return main frequencies and their fourier amplitudes, weakest first
def get_freqs(xgain, ygain, xphase, yphase, plot=False):
    feedback.set_delay(xi+xphase, 0, 0, yi+yphase)
    feedback.set_gain(xgain, 0, 0, ygain)
    feedback.send_parameters()
    time.sleep(2)

    while True:
        rec = feedback.playrec_frames((ding, nulls), nperiods=3072)
        fftx = np.fft.rfft(rec[0][600:])
        ffty = np.fft.rfft(rec[1][600:])
        # truncate
        fftx = np.abs(fftx[0:len(fftx)//32])
        ffty = np.abs(ffty[0:len(ffty)//32])

        lb = 950*2*32*len(fftx)//fs
        ub = 990*2*32*len(fftx)//fs
        peaksx = find_peaks(fftx, height=1)
        peaksy = find_peaks(ffty, height=1)
        main_peaksx = peaksx[0][peaksx[1]["peak_heights"].argsort()]
        main_peaksy = peaksy[0][peaksy[1]["peak_heights"].argsort()]
        main_peaksx = main_peaksx[(lb < main_peaksx) & (main_peaksx < ub)][-2:]
        main_peaksy = main_peaksy[(lb < main_peaksy) & (main_peaksy < ub)][-2:]

        if plot:
            plt.plot(rec[0])
            plt.plot(rec[1])
            plt.show()
            plt.plot(np.fft.fftfreq(fftx.size, d=64/fs), fftx)
            plt.plot(np.fft.fftfreq(ffty.size, d=64/fs), ffty)
            plt.scatter(main_peaksx*fs/2/32/fftx.size,
                fftx[main_peaksx], marker='x')
            plt.scatter(main_peaksy*fs/2/32/ffty.size,
                ffty[main_peaksy], marker='x')
            plt.yscale("log")
            plt.show()

        main_peaks_hz = fs*main_peaksy/ffty.size/2/32
        if main_peaksx.size < 2 or main_peaksy.size < 2:
            print("couldn't find two peaks")
            continue
        if np.abs(main_peaks_hz[1] - main_peaks_hz[0]) < 2:
            print("peaks are unreasonably close:", main_peaks_hz)
            continue
        return (fs*main_peaksy/ffty.size/2/32,
                np.array([ffty[main_peaksy[0]], ffty[main_peaksy[1]]])
        )

# adjust ygain until slightly below threshold
def ygain_below(ygain_min=0, ygain_max=2, ygain=1):
    xgain = feedback.get_gain()[0]
    while ygain_max - ygain_min > 0.01:
        feedback.set_gain(0, 0, 0, 0)
        feedback.send_parameters()
        time.sleep(1)
        feedback.set_gain(xgain, 0, 0, ygain)
        feedback.send_parameters()
        time.sleep(1)
        recy2 = 1E-9 * np.array(
            feedback.playrec_frames((ding, nulls), nperiods=1024)[1]
        )
        recy2 **= 2
        mag_first = sum(recy2[0:len(recy2)//2])
        mag_last = sum(recy2[len(recy2)//2:])
        print("first and last halves:", mag_first, mag_last)
        if mag_first > 2*mag_last:
            ygain_min = ygain
        else:
            ygain_max = ygain
        ygain = (ygain_min + ygain_max)/2
    return ygain_min

# adjust yphase (and ygain) until peaks are equal near threshold
def yphase_match(xgain, yphase=0):
    yphase_min = -yi
    yphase_max = yi
    feedback.set_gain(xgain, 0, 0, 0)
    while yphase_max - yphase_min > 1:
        assert(yphase_min < yphase and yphase < yphase_max)
        feedback.set_delay(xi, 0, 0, yi+yphase)
        ygain = ygain_below(ygain=feedback.get_gain()[3])
        freqs = get_freqs(xgain, ygain, 0, yphase)
        print(freqs)
        if freqs[0][0] < freqs[0][1]:   # weaker freq is lower
            yphase_min = yphase
            yphase += 1
        else:   # weaker freq is higher
            yphase_max = yphase
            yphase -= 1
    return yphase

xgains = -np.arange(0, 1, 0.1)
freqs = []
yphase = -30
for xgain in xgains:
    yphase = yphase_match(xgain, yphase)
    ygain = feedback.get_gain()[3]
    freqs += [get_freqs(xgain, ygain, 0, yphase)[0]]

print(xgains)
print(freqs)

