#!/usr/bin/env python3
# this program watches (but does not start new!) recordings in XY plot
import matplotlib.animation as animation
import matplotlib.pyplot as plt
import numpy as np
import os
import time
from qlab_alsaloop import Feedback

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

xs = []
ys = []

def animate(i: int):
    while not os.path.exists("recording.bin.done"):
        # wait for recording to finish
        pass
    xs, ys = map(
        lambda a: np.array(a,dtype="f8") * Feedback.mV,
        Feedback.get_recording()
    )
    xxip = float(np.dot(xs, xs))
    yyip = float(np.dot(ys, ys))
    xyip = float(np.dot(xs, ys))
    print(abs(xyip) / (xxip*xxip + yyip*yyip)**0.5)
    ax.clear()
    ax.plot(xs[0:1000], ys[0:1000], ".-")
    ax.set_aspect(1)

if __name__ == "__main__":
    _ = animation.FuncAnimation(fig, animate, interval=5000)
    plt.show()

