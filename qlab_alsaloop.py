# this is a python wrapper for qlab-alsaloop.c
from ctypes import *
import math
import numpy as np
import os
import signal
import subprocess
import threading
import time

BYTES_PER_FRAME = 8

class gain_matrix(Structure):
    _fields_ = [("xx", c_float), ("xy", c_float),
                ("yx", c_float), ("yy", c_float)]

class filter_coeffs(Structure):
    _fields_ = [("ax", c_float), ("bx", c_float), ("rax", c_float),
                ("ay", c_float), ("by", c_float), ("ray", c_float)]

# loop params
class loop_params_t(Structure):
    _fields_ = [("psize", c_uint32),
                ("rec_periods", c_uint16),
                ("play_periods", c_uint16),
                ("gain_matrix", gain_matrix),
                ("filter_coeffs", filter_coeffs),
                ("delay_xx", c_uint8),
                ("delay_xy", c_uint8),
                ("delay_yx", c_uint8),
                ("delay_yy", c_uint8)]


class Feedback:
    # parameters
    SAMPLE_RATE = 192000
    MAX_PSIZE = 1024
    MAX_REC_PERIODS = 8192
    SAVED_FRAMES = 256

    # constants
    mV = 1.389373796494296E-8   # mV per integer step (TODO: calibrate)
    AXIS_XX = 0     # specify XX matrix element
    AXIS_XY = 1     # specify XY matrix element
    AXIS_YX = 2     # specify YX matrix element
    AXIS_YY = 3     # specify YY matrix element


    def __init__(self):
        print("initializing feedback process")
        self._holding_amp = False
        self.loop_params = loop_params_t()
        self.loop_params.rec_periods = 0
        self.loop_params.play_periods = 0
        self.thread_lock = threading.Lock()     # mutex for threads
        self.childprocess = subprocess.Popen(['./qlab-alsaloop'],
                                             stdin=subprocess.PIPE)
        subprocess.Popen(["taskset", "-p", "-c", "3",
                         str(self.childprocess.pid)])
        time.sleep(0.5) # TODO: this is lazy but may be necessary?

    def __del__(self):
        print("feedback destructor called; sending SIGINT from python")
        self.childprocess.send_signal(signal.SIGINT)
        self.childprocess.wait()

    def set_psize(self, psize:int) -> None:
        if psize > self.MAX_PSIZE:
            raise ValueError("max psize is {} frames".format(self.MAX_PSIZE))
        self.loop_params.psize = psize

    def set_gain(self, xx, xy, yx, yy) -> None:
        self.loop_params.gain_matrix.xx = xx
        self.loop_params.gain_matrix.xy = xy
        self.loop_params.gain_matrix.yx = yx
        self.loop_params.gain_matrix.yy = yy

    # old version for compatibility
    def set_delay(self, dx, dy) -> None:
        assert(dx < self.SAVED_FRAMES and dy < self.SAVED_FRAMES)
        self.loop_params.delay_xx = dx
        self.loop_params.delay_xy = dx
        self.loop_params.delay_yx = dy
        self.loop_params.delay_yy = dy

    def set_delay(self, dxx, dxy, dyx, dyy) -> None:
        assert(dxx < self.SAVED_FRAMES and dxy < self.SAVED_FRAMES
            and dyx < self.SAVED_FRAMES and dyy < self.SAVED_FRAMES)
        self.loop_params.delay_xx = dxx
        self.loop_params.delay_xy = dxy
        self.loop_params.delay_yx = dyx
        self.loop_params.delay_yy = dyy

    def get_gain(self) -> (float, float, float, float):
        return (self.loop_params.gain_matrix.xx,
            self.loop_params.gain_matrix.xy,
            self.loop_params.gain_matrix.yx,
            self.loop_params.gain_matrix.yy
        )

    # like set_gain, but with change in gain
    def adjust_gain(self, xx, xy, yx, yy) -> None:
        self.loop_params.gain_matrix.xx += xx
        self.loop_params.gain_matrix.xy += xy
        self.loop_params.gain_matrix.yx += yx
        self.loop_params.gain_matrix.yy += yy

    def set_filter(self, ax, bx, rax, ay, by, ray) -> None:
        self.loop_params.filter_coeffs.ax = ax
        self.loop_params.filter_coeffs.bx = bx
        self.loop_params.filter_coeffs.rax = rax
        self.loop_params.filter_coeffs.ay = ay
        self.loop_params.filter_coeffs.by = by
        self.loop_params.filter_coeffs.ray = ray

    def set_recording(self, nperiods) -> None:
        self.loop_params.rec_periods = nperiods

    def send_parameters(self) -> None:
        self.childprocess.stdin.write(bytes(self.loop_params))
        self.childprocess.stdin.flush()

    def get_frames(self, nperiods:int=1) -> tuple[list[int], list[int]]:
        """Return a small amount of frames as two lists for x and y. Long
        recordings of data will use lots of memory!"""
        self.thread_lock.acquire()  # prevent FileNotFoundError race condition
        if os.path.exists("recording.bin.done"):
            os.remove("recording.bin.done")
        self.thread_lock.release()
        self.set_recording(nperiods)
        self.send_parameters()
        ret = self.get_recording()
        self.loop_params.rec_periods = 0
        return ret

    @staticmethod   # can be called without instantiating a Feedback
    def get_recording() -> tuple[list[int], list[int]]:
        """Like get_frames, but will NOT write recording periods or send
        parameters. Will only look for recording file."""
        while not os.path.exists("recording.bin.done"):
            # wait for recording to finish
            time.sleep(0.001)
        retx = []
        rety = []
        with open("recording.bin", "rb") as f:
            data = f.read()
            bps = BYTES_PER_FRAME // 2   # bytes per sample
            for i in range(0, len(data), BYTES_PER_FRAME):
                retx.append(int.from_bytes(data[i:i+bps],
                            byteorder="little", signed=True))
                rety.append(int.from_bytes(data[i+bps:i+BYTES_PER_FRAME],
                            byteorder="little", signed=True))
        return (retx, rety)

    def playrec_frames(self, play_frames:tuple[list[int],list[int]],
        nperiods:int=1, newfile:bool=True
    ) -> tuple[list[int],list[int]]:
        """Sum the signal play_frames into the feedback after the gain matrix,
        and then return the recorded response."""
        N = len(play_frames[0])
        assert N == len(play_frames[1])
        if newfile:
            with open("modulation.bin", "wb") as f:
                # interleave the two arrays
                ints = np.vstack(play_frames).reshape((-1,),order='F')
                f.write(ints.tobytes())
#                # this might be slow
#                for i in range(N):
#                    f.write(play_frames[0][i].to_bytes(4, "little",
#                        signed=True))
#                    f.write(play_frames[1][i].to_bytes(4, "little",
#                        signed=True))
        self.loop_params.play_periods = math.ceil(N / self.loop_params.psize)
        print("calling get_frames()")
        ret = self.get_frames(nperiods)
        self.loop_params.play_periods = 0
        return ret

    def get_rms_mv(self, nperiods:int=1) -> (float, float):
        """Get the x and y amplitudes of the oscillation in mV rms, measured
        across <nperiods> periods."""
        xarr, yarr = self.get_frames(nperiods)
        xarr = np.array(xarr, dtype="f8")
        yarr = np.array(yarr, dtype="f8")
        xrms = self.mV * np.sqrt(np.dot(xarr, xarr)/len(xarr))
        yrms = self.mV * np.sqrt(np.dot(yarr, yarr)/len(yarr))
        return (xrms, yrms)

    def tune_coarse_phase(self, axis:int, freq:int,
        periods:int=512, discard:int=256, mindelay:int=0, maxdelay:int=0,
        quiet:bool=True,
    ) -> tuple[int,int]:
        """Tune the delay to reach zero phase shift (mod 2π) at the given drive
        frequency `freq`. Play a cosine for `periods` periods, throwing away the
        first `discard` frames from the recording, and then check delays between
        `mindelay` and `maxdelay` to maximize the inner product."""
        phi = (np.arange(periods*self.loop_params.psize)
            * 2*np.pi*freq/self.SAMPLE_RATE)
        src = (2**28 * np.cos(phi)).astype(np.int32)
        zer = np.zeros(periods*self.loop_params.psize).astype(np.int32)
        maxdelay = maxdelay if maxdelay else self.loop_params.psize

        print("playing test waveform")
        if axis == self.AXIS_XX or axis == self.AXIS_XY:
            rec = self.playrec_frames((src, zer), nperiods=periods)
        elif axis == self.AXIS_YX or axis == self.AXIS_YY:
            rec = self.playrec_frames((zer, src), nperiods=periods)
        else:
            raise ValueError("please specify one of the four AXIS_* values")
        print("played test waveform")

        if axis == self.AXIS_XX or axis == self.AXIS_YX:
            rec_ind = 0
        elif axis == self.AXIS_XY or axis == self.AXIS_YY:
            rec_ind = 1

        fomdict = {}
        for delay in range(mindelay, maxdelay):
            # throw away first `discard` frames, then adjust for delay
            if delay == 0:
                seg_src = np.asarray(src[discard:], dtype="f8")
                seg_rec = np.asarray(rec[rec_ind][discard:], dtype="f8")
#                seg_rec = np.asarray(rec[rec_ind][discard:], dtype="f8")
#                seg_src = np.asarray(src[discard:], dtype="f8")
            else:
                seg_src = np.asarray(src[discard+delay:], dtype="f8")
                seg_rec = np.asarray(rec[rec_ind][discard:-delay], dtype="f8")
#                seg_rec = np.asarray(rec[rec_ind][discard+delay:], dtype="f8")
#                seg_src = np.asarray(src[discard:-delay], dtype="f8")
            ip_src = np.dot(seg_src, seg_src)
            ip_rec = np.dot(seg_rec, seg_rec)
            fom = np.dot(seg_src, seg_rec) / np.sqrt(ip_src * ip_rec)
            fomdict[delay] = fom
            if not quiet:
                print("delay of {} => fom of {}".format(delay, fom))

        fomdict = dict(sorted(fomdict.items(), key=lambda x: x[1]))
        print("best delay {} worst delay {}".format(
                list(fomdict)[-1], list(fomdict)[0])
        )
        return (list(fomdict)[-1], list(fomdict)[0])

    def tune_fine_phase(self, precision:int=10, rms_periods:int=5) -> None:
        """Phase shift via linear interpolation to reach the maximum amplitude
        at the CURRENT gain and recursive filter parameters, with <precision>
        steps between two samples."""
        # X CHANNEL
        rmsdict = {}
        for step in range(precision):
            self.loop_params.filter_coeffs.ax = step/precision
            self.loop_params.filter_coeffs.bx = 1 - step/precision
            self.send_parameters()
            time.sleep(0.5)
            rmsdict[step] = self.get_rms_mv(rms_periods)[0]
            print("ax of {} got rms of {}".format(step/precision,
                                                     rmsdict[step]))
        beststep = max(rmsdict, key=rmsdict.get)
        print("setting ax={} and bx={}".format(beststep/precision,
                                               1-beststep/precision))
        self.loop_params.filter_coeffs.ax = beststep/precision
        self.loop_params.filter_coeffs.bx = 1 - beststep/precision
        # Y CHANNEL
        rmsdict = {}
        for step in range(precision):
            self.loop_params.filter_coeffs.ax = step/precision
            self.loop_params.filter_coeffs.bx = 1 - step/precision
            self.send_parameters()
            time.sleep(0.5)
            rmsdict[step] = self.get_rms_mv(rms_periods)[1]
            print("ay of {} got rms of {}".format(step/precision,
                                                     rmsdict[step]))
        beststep = max(rmsdict, key=rmsdict.get)
        print("setting ay={} and by={}".format(beststep/precision,
                                               1-beststep/precision))
        self.loop_params.filter_coeffs.ay = beststep/precision
        self.loop_params.filter_coeffs.by = 1 - beststep/precision
        self.send_parameters()
        return None

    def __hold_amp(self, rms_mv, tolerance:float, step_xx:float, step_xy:float,
                   step_yx:float, step_yy:float, wait_time):
        prev_rms = [0, 0]
        while self._holding_amp:
            # use the default get_rms_mv nperiods for now!
            rms = self.get_rms_mv()
            print("recorded rms of ({}, {})".format(rms[0], rms[1]))
            if rms[0] - rms_mv > tolerance * rms_mv and rms[0] > prev_rms[0]:
                # decrease x gain
                self.loop_params.gain_matrix.xy -= step_xy
                self.loop_params.gain_matrix.xx -= step_xx
            elif rms_mv - rms[0] > tolerance * rms_mv and rms[0] < prev_rms[0]:
                # increase x gain
                self.loop_params.gain_matrix.xy += step_xy
                self.loop_params.gain_matrix.xx += step_xx
            if rms[1] - rms_mv > tolerance * rms_mv and rms[1] > prev_rms[1]:
                # decrease y gain
                self.loop_params.gain_matrix.yx -= step_yx
                self.loop_params.gain_matrix.yy -= step_yy
            elif rms_mv - rms[1] > tolerance * rms_mv and rms[1] < prev_rms[1]:
                # increase y gain
                self.loop_params.gain_matrix.yx += step_yx
                self.loop_params.gain_matrix.yy += step_yy
            # TODO: make sure not to cross zero when doing this
            print("setting gains to xx={}, xy={}, yx={}, yy={}".format(
                  self.loop_params.gain_matrix.xx,
                  self.loop_params.gain_matrix.xy,
                  self.loop_params.gain_matrix.yx,
                  self.loop_params.gain_matrix.yy))
            prev_rms = rms
            self.send_parameters()
            time.sleep(wait_time)

    def start_hold_amp(self, rms_mv, tolerance:float=0.05,
                       step_xx:float=0, step_xy:float=0.005,
                       step_yx:float=0.005, step_yy:float=0,
                       wait_time=0.5
                       ) -> None:
        """Start trying to hold a certain mV amplitude at the CURRENT gain and
        filter parameters, with a fractional tolerance of <tolerance>, stepping
        gain by step_x and step_y at a time, waiting for <wait_time> seconds
        between checking. If you want one gain to be negative, set the step to a
        negative value!"""
        if self._holding_amp:
            print("already holding amplitude! ignoring start_hold_amp")
            return None
        self._holding_amp = True
        self.hold_amp_thread = threading.Thread(target=self.__hold_amp,
                                                args=(rms_mv,
                                                      tolerance,
                                                      step_xx,
                                                      step_xy,
                                                      step_yx,
                                                      step_yy,
                                                      wait_time))
        self.hold_amp_thread.daemon = True  # die when main thread dies
        self.hold_amp_thread.start()
        print("amplitude holding started with target of {} mV".format(rms_mv))
        return None

    def stop_hold_amp(self) -> None:
        """Stop any secondary feedback, i.e. holding a certain amplitude."""
        if self._holding_amp:
            self._holding_amp = False
            self.hold_amp_thread.join(10)   # ten second timeout
            if self.hold_amp_thread.is_alive():
                raise RuntimeError("amplitude holding thread failed to join()")
            else:
                print("amplitude holding stopped")
        else:
            print("amplitude holding was not running! ignoring stop_hold_amp")
        return None

