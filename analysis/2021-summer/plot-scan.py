#!/usr/bin/env python3
import matplotlib.pyplot as plt
import pandas as pd
from scipy import optimize

# magnitude of phase of oscillation relative to drive as a function of angular
# frequency f, drive phasor F, loss factor gamma, and natural frequency f0:
def mag_phasor(f:float, F:float, gamma:float, f0:float) -> float:
    return abs(F/(f**2 - 1j*gamma*f - f0**2))

scanx = pd.read_csv("scanx.csv", header=None, names=["f", "re", "im"])
scany = pd.read_csv("scany.csv", header=None, names=["f", "re", "im"])

xmags = []
ymags = []
for i in range(len(scanx["f"])):
    xmags.append((scanx["re"][i]**2 + scanx["im"][i]**2)**0.5)
for i in range(len(scany["f"])):
    ymags.append((scany["re"][i]**2 + scany["im"][i]**2)**0.5)

guess = (30, 16, 7E3)
# fit to Re[x]
xparam, _ = optimize.curve_fit(mag_phasor, scanx["f"], xmags, p0=guess,
                               maxfev=80000)
yparam, _ = optimize.curve_fit(mag_phasor, scany["f"], ymags, p0=guess,
                               maxfev=80000)
#yparam_im, _ = optimize.curve_fit(im_phasor, yfs, yims, p0=guess, maxfev=8000)
print("x (F, gamma, f0) measured from magnitude fit: {}".format(xparam))
print("y (F, gamma, f0) measured from magnitude fit: {}".format(yparam))

plt.plot(scanx["f"], scanx["re"], label="Re[X]")
plt.plot(scanx["f"], scanx["im"], label="Im[X]")
plt.plot(scany["f"], scany["re"], label="Re[Y]")
plt.plot(scany["f"], scany["im"], label="Im[Y]")
plt.xlabel("f (Hz)")
plt.ylabel("re, im (V)")
plt.legend()
plt.show()

