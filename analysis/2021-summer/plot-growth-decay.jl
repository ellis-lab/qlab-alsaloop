#!/usr/bin/julia
using CairoMakie

struct Frame
    ch1::Int32
    ch2::Int32
end
function Base.read(io::IO, ::Type{Frame})
    Frame(read(io,Int32),read(io,Int32))
end

mV = 1.389373796494296E-8   # mV per integer step
fs = 192000

decay_x = Float32[]
decay_y = Float32[]
growth_x = Float32[]
growth_y = Float32[]
open("rec-down.bin", "r") do io
    while !eof(io)
        push!(decay_x, read(io, Frame).ch1 * mV)
        push!(decay_y, read(io, Frame).ch2 * mV)
    end
end
open("rec-up.bin", "r") do io
    while !eof(io)
        push!(growth_x, read(io, Frame).ch1 * mV)
        push!(growth_y, read(io, Frame).ch2 * mV)
    end
end
println("data imported")

fig1 = Figure(resolution=(2048, 1024), font="Iosevka")
fig2 = Figure(resolution=(2048, 1024), font="Iosevka")
fig3 = Figure(resolution=(2048, 1024), font="Iosevka")
fig4 = Figure(resolution=(2048, 1024), font="Iosevka")
ax_dx = fig1[1, 1] = Axis(fig1, title="oscillation decay, x")
ax_dy = fig2[1, 1] = Axis(fig2, title="oscillation decay, y")
ax_gx = fig3[1, 1] = Axis(fig3, title="oscillation growth, x")
ax_gy = fig4[1, 1] = Axis(fig4, title="oscillation growth, y")
for ax in [ax_dx, ax_dy]
    ax.xlabel = "time after stopping secondary feedback (s)"
    ax.ylabel = "amplitude (mV)"
    ax.xticks = 0:0.5:12
    ax.yticks = -6:0.5:6
end
for ax in [ax_gx, ax_gy]
    ax.xlabel = "time after stopping secondary feedback (s)"
    ax.ylabel = "amplitude (mV)"
    ax.xticks = 0:0.5:12
    ax.yticks = -30:2:30
end
println("figures are set up")

td = range(0, step=1/fs, length=length(decay_x))
task1 = Threads.@spawn lines!(ax_dx, td, decay_x, color=:red)
task2 = Threads.@spawn lines!(ax_dy, td, decay_y, color=:blue)
tg = range(0, step=1/fs, length=length(growth_x))
task3 = Threads.@spawn lines!(ax_gx, tg, growth_x, color=:red)
task4 = Threads.@spawn lines!(ax_gy, tg, growth_y, color=:blue)
for task in [task1, task2, task3, task4]
    wait(task)
end
println("plots generated")

task1 = Threads.@spawn save("decay_x.png", fig1)
task2 = Threads.@spawn save("decay_y.png", fig2)
task3 = Threads.@spawn save("growth_x.png", fig3)
task4 = Threads.@spawn save("growth_y.png", fig4)
for task in [task1, task2, task3, task4]
    wait(task)
end
println("figures saved to png")

