#!/usr/bin/julia
using ArgParse
using Plots; gr()

argset = ArgParseSettings()
@add_arg_table argset begin
    "path"
        help = "path to recording file"
        required = true
end
args = parse_args(argset)

struct Frame
    ch1::Int32
    ch2::Int32
end
function Base.read(io::IO, ::Type{Frame})
    Frame(read(io,Int32),read(io,Int32))
end

mV = 1.389373796494296E-8   # mV per integer step
fs = 192000

x = Float32[]
y = Float32[]
open(args["path"], "r") do io
    seek(io, trunc(Int, 2.25*fs)*sizeof(Frame))
    for i in 1:0.03*fs
        push!(x, read(io, Frame).ch1 * mV)
        push!(y, read(io, Frame).ch2 * mV)
    end
end
println("data imported")

c = range(0, stop=1, length=length(x))
default(fontfamily="computer modern")
scalefontsizes(4)
p1 = plot(x, y, aspect_ratio=:equal,
          title="oscillator response to excitation",
          xlims=(-30, 30), ylims=(-30, 30),
          xlabel="x (mV)", ylabel="y (mV)",
          line_z=c, legend=false,
          size=(2048,2048))
println("graph plotted")
savefig("xy.png")
println("saved to xy.png")

