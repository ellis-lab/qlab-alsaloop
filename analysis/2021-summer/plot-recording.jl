#!/usr/bin/julia
using ArgParse
using CairoMakie

argset = ArgParseSettings()
@add_arg_table argset begin
    "path"
        help = "path to recording file"
        required = true
end
args = parse_args(argset)

struct Frame
    ch1::Int32
    ch2::Int32
end
function Base.read(io::IO, ::Type{Frame})
    Frame(read(io,Int32),read(io,Int32))
end

mV = 1.389373796494296E-8   # mV per integer step
fs = 192000

x = Float32[]
y = Float32[]
open(args["path"], "r") do io
    while !eof(io)
        push!(x, read(io, Frame).ch1 * mV)
        push!(y, read(io, Frame).ch2 * mV)
    end
end
println("data imported")

fig = Figure(resolution=(2048, 1024), font="Iosevka")
ax1 = fig[1, 1] = Axis(fig, title="oscillation")
ax1.xlabel = "time (s)"
ax1.ylabel = "amplitude (mV)"

t = range(0, step=1/fs, length=length(x))
x = lines!(ax1, t, x, color=:red)
#y = lines!(ax1, t, y, color=:blue)
println("graph plotted")

save("plot.png", current_figure())
println("figure saved to plot.png")

