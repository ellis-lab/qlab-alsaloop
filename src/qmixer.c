#define _GNU_SOURCE
#include <err.h>
#include <libgen.h>
#include <ncurses.h>
#include <sched.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "qlab-alsaloop.h"

#define F_LERP_STEP	0
#define F_PSIZE		1
#define F_PREC		2
#define F_DELAY_XX	3
#define F_DELAY_XY	4
#define F_DELAY_YX	5
#define F_DELAY_YY	6
#define F_LERP_BX	7
#define F_LERP_BY	8
#define F_GAIN_STEP	9
#define F_GXX		10
#define F_GXY		11
#define F_GYX		12
#define F_GYY		13
#define N_FIELDS	14

#define INIT_PSIZE 256

rec_status_t rec_status;

// initialize loop parameters with some default values
loop_params_t loop_params = {
	.psize = INIT_PSIZE,
	.rec_periods = 0,
	.gain_matrix = {
		.xx = 0,	.xy = 1,
		.yx = -1,	.yy = 0,
	},
	.filter_coeffs = {
		.ax = 1, .bx = 0, .rax = 0,
		.ay = 1, .by = 0, .ray = 0,
	},
	.delay_xx = 0, .delay_xy = 0,
	.delay_yx = 0, .delay_yy = 0,
};

const char *field_names[N_FIELDS];
float fields[N_FIELDS] = {0.0};

int childpid;
int pipefd[2];	// [0] is read end, [1] is write end
FILE *control;	// file pointer piped to qlab-alsaloop program's stdin


int adjust_params()
{
	// set parameters locally
	loop_params.psize = (uint32_t)fields[F_PSIZE];
	loop_params.rec_periods = (uint32_t)fields[F_PREC];
	loop_params.gain_matrix.xx = fields[F_GXX];
	loop_params.gain_matrix.xy = fields[F_GXY];
	loop_params.gain_matrix.yx = fields[F_GYX];
	loop_params.gain_matrix.yy = fields[F_GYY];
	loop_params.filter_coeffs.bx = fields[F_LERP_BX];
	loop_params.filter_coeffs.ax = 1 - fields[F_LERP_BX];
	loop_params.filter_coeffs.by = fields[F_LERP_BY];
	loop_params.filter_coeffs.ay = 1 - fields[F_LERP_BY];
	loop_params.delay_xx = fields[F_DELAY_XX];
	loop_params.delay_xy = fields[F_DELAY_XY];
	loop_params.delay_yx = fields[F_DELAY_YX];
	loop_params.delay_yy = fields[F_DELAY_YY];

	// send parameters
	size_t n = fwrite(&loop_params, sizeof(loop_params_t), 1, control);
	if (n != 1)
		err(EXIT_FAILURE, "short write to control (%d)", n);
	fflush(control);
	return 0;
}


int print_fields(WINDOW *win, int selected_field)
{
	for (int i=0; i<N_FIELDS; i++) {
		if (selected_field == i) {
			wattron(win, A_REVERSE);
			mvwprintw(win, i+1, 1, field_names[i]);
			wattroff(win, A_REVERSE);
		} else {
			mvwprintw(win, i+1, 1, field_names[i]);
		}
		mvwprintw(win, i+1, 24, "%F", fields[i]);
	}
	wrefresh(win);
	return 0;
}


int main(int argc, char *argv[])
{
	if (argc != 1) {
		puts("No arguments, please.");
		return -1;
	}
	initscr();
	raw();
	keypad(stdscr, TRUE);
	noecho();
	addstr(
	"===================== WELCOME TO QMIXER :) ====================\n"
	"             use hjkl or arrow keys to set values              \n"
	"               space to send params, q to quit                 \n"
	);
	refresh();

	field_names[F_LERP_STEP] = " lerp step (frames):";
	field_names[F_PSIZE]     = "        period size:";
	field_names[F_PREC]      = "     record periods:";
	field_names[F_DELAY_XX]  = "     sample delay xx:";
	field_names[F_DELAY_XY]  = "     sample delay xy:";
	field_names[F_DELAY_YX]  = "     sample delay yx:";
	field_names[F_DELAY_YY]  = "     sample delay yy:";
	field_names[F_LERP_BX]   = "            lerp bx:";
	field_names[F_LERP_BY]   = "            lerp by:";
	field_names[F_GAIN_STEP] = "          gain step:";
	field_names[F_GXX]       = "                Gxx:";
	field_names[F_GXY]       = "                Gxy:";
	field_names[F_GYX]       = "                Gyx:";
	field_names[F_GYY]       = "                Gyy:";
	fields[F_LERP_STEP] = 0.1;
	fields[F_PSIZE] = INIT_PSIZE;
	fields[F_GAIN_STEP] = 0.1;
	WINDOW *win = newwin(N_FIELDS+3, 61, 3, 1);
	box(win, 0, 0);
	int done = 0;
	int selected_field = F_LERP_STEP;
	print_fields(win, 0);
	mvprintw(17, 0, "starting feedback ... log messages below:\n");

	// set up our pipe
	if (pipe(pipefd))
		err(EXIT_FAILURE, "couldn't create pipe");

	childpid = fork();
	if (childpid == 0) {
		// first deal with stupid path stuff (we want to call
		// qlab-alsaloop regardless of cwd)
		char *libdir = dirname(argv[0]);
		char childpath[1024];
		strncpy(childpath, libdir, 1024-sizeof("/qlab-alsaloop"));
		strcat(childpath, "/qlab-alsaloop");
		// we are child, so set up IPC
		dup2(pipefd[0], STDIN_FILENO);
		close(pipefd[0]);
		close(pipefd[1]);
		// TODO: pipe output to a fifo file somewhere instead of
		// polluting the tui
		// set up cpu affinity and run
		cpu_set_t cpumask;
		CPU_ZERO(&cpumask);
		CPU_SET(3, &cpumask);
		if (sched_setaffinity(0, sizeof(cpumask), &cpumask))
			warn("couldn't set cpu affinity:");
		//execl("/usr/bin/echo", "/usr/bin/echo", (char *)NULL);
		execl(childpath, childpath, (char *)NULL);
		// and our feedback is away!
	} else {
		close(pipefd[0]);
		control = fdopen(pipefd[1], "w");
		if (!control)
			err(EXIT_FAILURE, "failed to open control stream");
	}

	while (!done) {		// main loop
		switch (getch()) {
		case ' ':
			adjust_params();
			break;
		case 'h':
		case KEY_LEFT:
			if (selected_field == F_LERP_STEP
			|| selected_field == F_GAIN_STEP) {
				// if it's a step field, adjust multiple of ten
				fields[selected_field] /= 10;
			} else if (selected_field == F_PSIZE
			|| selected_field == F_PREC
			|| selected_field == F_DELAY_XX
			|| selected_field == F_DELAY_XY
			|| selected_field == F_DELAY_YX
			|| selected_field == F_DELAY_YY) {
				// whole-sample adjust
				if (fields[selected_field] > 0)
					fields[selected_field] -= 1;
			} else if (selected_field == F_LERP_BX
			|| selected_field == F_LERP_BY) {
				// lerp adjust
				if (fields[selected_field]
				>= fields[F_LERP_STEP]) {
					fields[selected_field]
					-= fields[F_LERP_STEP];
				}
			} else {
				// gain adjust
				fields[selected_field] -= fields[F_GAIN_STEP];
			}
			break;
		case 'j':
		case KEY_DOWN:
			if (selected_field < N_FIELDS-1)
				selected_field++;
			break;
		case 'k':
		case KEY_UP:
			if (selected_field > 0)
				selected_field--;
			break;
		case 'l':
		case KEY_RIGHT:
			if (selected_field == F_LERP_STEP
			|| selected_field == F_GAIN_STEP) {
				// if it's a step field, adjust multiple of ten
				fields[selected_field] *= 10;
			} else if (selected_field == F_PSIZE) {
				// whole-sample adjust
				if (fields[selected_field] < MAX_PSIZE)
					fields[selected_field] += 1;
			} else if (selected_field == F_PREC) {
				// whole-sample adjust
				if (fields[selected_field] < MAX_REC_PERIODS)
					fields[selected_field] += 1;
			} else if (selected_field == F_DELAY_XX
			|| selected_field == F_DELAY_XY
			|| selected_field == F_DELAY_YX
			|| selected_field == F_DELAY_YY) {
				// whole-sample adjust
				if (fields[selected_field] < SAVED_FRAMES - 2)
					fields[selected_field] += 1;
			} else if (selected_field == F_LERP_BX
			|| selected_field == F_LERP_BY) {
				// lerp adjust
				if (fields[selected_field]
				<= 1 - fields[F_LERP_STEP]) {
					fields[selected_field]
					+= fields[F_LERP_STEP];
				}
			} else {
				// gain adjust
				fields[selected_field] += fields[F_GAIN_STEP];
			}
			break;
		case 'q':
			done = 1;
			break;
		}
		print_fields(win, selected_field);
		refresh();
	}

	// cleanup
	endwin();
	fclose(control);
	close(pipefd[1]);
	kill(childpid, SIGINT);
	return 0;
}

