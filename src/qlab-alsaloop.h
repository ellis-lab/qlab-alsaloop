#ifndef QLAB_ALSALOOP_H
#define QLAB_ALSALOOP_H

#define ALSA_PCM_NEW_HW_PARAMS_API
#include <alsa/asoundlib.h>

// maximum frames per period: determines buffer size and alsa period size
#define MAX_PSIZE 1024
#define BYTES_PER_FRAME 8	// 2 channels, 32 bits per sample
#define SAMPLE_RATE 192000	// frames per second
#define SAVED_FRAMES 256	// how many frames we save from the last period
// TODO: check with a runtime assert that this contains at least delay+1 frames
// with our current parameters
#define MAX_REC_PERIODS 8192	// maximum periods that can be recorded
#define SCHED_PRIORITY 95

// c preprocessor magic (https://stackoverflow.com/a/809465)
#define CASSERT(predicate, file) _impl_CASSERT_LINE(predicate,__LINE__,file)
#define _impl_PASTE(a,b) a##b
#define _impl_CASSERT_LINE(predicate, line, file) \
	typedef char _impl_PASTE(assertion_failed_##file##_,line)\
		[2*!!(predicate)-1];

#define ANSI_COLOR_RED "\x1b[31m"
#define ANSI_COLOR_RESET "\x1b[0m"

typedef struct {
	int32_t ch1;
	int32_t ch2;
} frame_t;

CASSERT(sizeof(uint32_t) == sizeof(snd_pcm_uframes_t), qlab_alsaloop_c);

// TODO: add an xrunabort boolean, which is switched on when experiment is in
// progress (can't abort on xruns from the beginning because timing won't be
// perfect during initialization)
typedef struct {
	// this is OUR period size, not alsa's. alsa's period size is always set
	// to MAX_PSIZE because changing the psize on the fly doesn't work well.
	// snd_pcm_uframes_t is uint32_t as asserted above
	uint32_t psize;
	// how many periods to record (0 for no recording)
	// max number of periods is 65535
	uint16_t rec_periods;
	uint16_t play_periods;
	struct gain_matrix {
		// xx and yy may be unnecessary as they can be implemented with
		// filter_coeffs instead. consider optimizing those away
		float xx;	float xy;
		float yx;	float yy;
	} gain_matrix;
	struct filter_coeffs {
		// P_k = a*S_k + b*S_{k-1} + ra*P_{k-1}
		// where r denotes recursive
		// more coefficients can be added later.
		// for a simple lerp delay, all coefficients are zero,
		// except a and b (a+b=1)
		float ax; float bx; float rax;
		float ay; float by; float ray;
	} filter_coeffs;
	uint8_t delay_xx; uint8_t delay_xy;
	uint8_t delay_yx; uint8_t delay_yy;
} loop_params_t;

// exit_thread state is to signal to the ipc worker that it should exit
enum playrec_state {todo, doing, done, exit_thread};
typedef struct {
	enum playrec_state state;
	uint16_t index;
	uint16_t bufcount;	// number of bufs for *this* recording
	frame_t *bufs[MAX_REC_PERIODS];		// array of buf_in pointers
	// TODO: this new recording is untested!!
} rec_status_t;

#endif	// from ifndef QLAB_ALSALOOP_H

