#!/bin/sh
set -e

# ensure we are on performance mode
for cpu in /sys/devices/system/cpu/cpu[0-9]*; do
	echo -n performance | sudo tee $cpu/cpufreq/scaling_governor
done
echo

